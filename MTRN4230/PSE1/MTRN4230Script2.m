close all;

%For detecting objects of a
% particular category, such as people or faces, see |vision.PeopleDetector| 
% and |vision.CascadeObjectDetector|.

%Read images
I=imread('IMG_0002.jpg'); imsharpen(I); %Chocolate image - front face
B=imread('Background.jpg'); %Table image to subtract background
T=imread('IMG_0084.jpg'); imsharpen(T);%Test image for finding chocolates

%hsv/gray conversion
% I2=rgb2hsv(I);
% T2=rgb2hsv(T);
% B2=rgb2hsv(B);

%Extract one of three composite matrices of images
I=I(:,:,1);
B=B(:,:,1);
T=T(:,:,1);


%Texture filtering
% I=rangefilt(I);
% B=rangefilt(B);
% T=rangefilt(T);

I=I(456:688,583:709); %Crop out extra border of chocolate bar
%I=I(1:100,1:127); %Crop out text on chocolate bar
I=imrotate(I,-90);

%I=I+45;
%T=T+45;
%B=B+45;

I=220-I; %Subtract chocolate image from average table value
S=B-T; %Subtract test image from brackground
%S=255-T;

%S=T-B; %for texture analysis
chocImage=I;
sceneImage=S;
sceneImage=sceneImage(225:1200,:); %Crop out conveyor and outside of table


%Detect features
chocPoints = detectSURFFeatures(chocImage);
scenePoints = detectSURFFeatures(sceneImage);
%Show overlay of points on chocolate image
figure;
imshow(chocImage);
title('12 Strongest Feature Points from Box Image');
hold on;
plot(selectStrongest(chocPoints, 12));
%Show overlay of points of scene image
figure;
imshow(sceneImage);
title('50 Strongest Feature Points from Scene Image');
hold on;
plot(selectStrongest(scenePoints, 50));


[chocFeatures, chocPoints] = extractFeatures(chocImage, chocPoints);
[sceneFeatures, scenePoints] = extractFeatures(sceneImage, scenePoints);
chocPairs = matchFeatures(chocFeatures, sceneFeatures);
matchedChocPoints = chocPoints(chocPairs(:, 1), :);
matchedScenePoints = scenePoints(chocPairs(:, 2), :);
figure;
showMatchedFeatures(chocImage, sceneImage, matchedChocPoints, ...
matchedScenePoints, 'montage');
title('Putatively Matched Points (Including Outliers)');
[tform, inlierChocPoints, inlierScenePoints] = ...
estimateGeometricTransform(matchedChocPoints, matchedScenePoints, 'similarity');
figure;
showMatchedFeatures(chocImage, sceneImage, inlierChocPoints, ...
    inlierScenePoints, 'montage');
title('Matched Points (Inliers Only)');

%% 
% Get the bounding polygon of the reference image.
boxPolygon = [1, 1;...                           % top-left
        size(chocImage, 2), 1;...                 % top-right
        size(chocImage, 2), size(chocImage, 1);... % bottom-right
        1, size(chocImage, 1);...                 % bottom-left
        1, 1];                   % top-left again to close the polygon

%%
% Transform the polygon into the coordinate system of the target image.
% The transformed polygon indicates the location of the object in the
% scene.
newBoxPolygon = transformPointsForward(tform, boxPolygon);    

%%
% Display the detected object.
figure;
imshow(sceneImage);
hold on;
line(newBoxPolygon(:, 1), newBoxPolygon(:, 2), 'Color', 'y');
title('Detected Box');


original = chocImage;distorted = sceneImage;
ptsOriginal  = detectSURFFeatures(original);
ptsDistorted = detectSURFFeatures(distorted);
[featuresOriginal,   validPtsOriginal]  = extractFeatures(original,  ptsOriginal);
[featuresDistorted, validPtsDistorted]  = extractFeatures(distorted, ptsDistorted);
indexPairs = matchFeatures(featuresOriginal, featuresDistorted);
matchedOriginal  = validPtsOriginal(indexPairs(:,1));
matchedDistorted = validPtsDistorted(indexPairs(:,2));
figure;
showMatchedFeatures(original,distorted,matchedOriginal,matchedDistorted);
title('Putatively matched points (including outliers)');
[tform, inlierDistorted, inlierOriginal] = estimateGeometricTransform(...
    matchedDistorted, matchedOriginal, 'similarity');
figure;
showMatchedFeatures(original,distorted, inlierOriginal, inlierDistorted);
title('Matching points (inliers only)');
legend('ptsOriginal','ptsDistorted');
Tinv  = tform.invert.T;

ss = Tinv(2,1);
sc = Tinv(1,1);
scale_recovered = sqrt(ss*ss + sc*sc)
theta_recovered = atan2(ss,sc)*180/pi


