% MTRN4230 PSE1 JAMES CARTHEW z3221278@student.unsw.edu.au
% MATLAB2013a
% 30 March 2015
function chocolate_detection
init();
file_name = 'IMG_0017.jpg';
image = ImageH(file_name);

% vid = videoinput('winvideo', 1, 'RGB24_1600x1200'); 
% frame = getsnapshot(vid);
% imshow(frame); % To display a single frame 
% preview(vid); % To display a live video stream

% Might put data into object (hopefully!!!)
% image.image_data = frame;

% Get some idea of where chocolate bars live
image.surf_descript(false);
% use those descriptors to block out some obvious
% empty areas. maybe it will help?
image.rc_remove_trivial_blocks(40, 0.2);

% image.surf_centroid();

image.dev_plot(1)


return;

function init()
% A Fresh Start is always nice...
close all
clear all
clc
return;
