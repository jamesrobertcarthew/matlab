% MTRN4230 PSE1 JAMES CARTHEW z3221278@student.unsw.edu.au
% MATLAB2013a
% 30 March 2015


function image_processing_operations()
    init()
    % You may not need this to run on your machine
    % See Note at end...
    run OrientedScalePointFeature.m
    % Utilities
    development = true; % development activates subplots for each individual image process
    fig = 1;
    disp('hi');
    % Constants
    ONION = 1;
    BOARD = 2;
    BALL = 3;
    HANDS = 4;
    FILENAMES = {'onion.png'; 'board.tif'; 'football.jpg';'hands1.jpg'};
    
    % Create and array of Image Objects
    % imgs array is unitialised but should be fine
    for k = 1:4
        imgs(k) = ImageH(FILENAMES{k});
    end
    
    % Flipping Board image purely for formatting
    imgs(BOARD).rotate_ninety()
    
    % Plot Unmodified images for presentation mode
    if development == false
        figure(1); hold on;
        j = 1;
        for k = 1:8
            if rem(k, 2) ~= 0
                imgs(j).sub_plot_image(8, 2, k);
                j = j+1;
            end
        end
    end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%    
    % PART 1/a
    % Grey scale all but blue in Board image
    if development
        % before
        figure(fig)
        imgs(BOARD).sub_plot_image(2, 1, 1)   
    end
    imgs(BOARD).color_seg(3, 70, 0.5);
    if development
        % after
        imgs(BOARD).sub_plot_image(2, 1, 2) 
        fig = fig + 1;
    end
    
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%    
    % PART 1/b
    % Outline hands1.jpg
    if development
        % before
        figure(fig)
        imgs(HANDS).sub_plot_image(2, 1, 1)
    end
    % a more homebrew method (slower and a little noisier)
    imgs(HANDS).trace(200, 300, 10);
    % faster, cleaner, boring-er method
%     imgs(HANDS).trace_2()
    if development
        % after
        imgs(HANDS).sub_plot_image(2, 1, 2)
        fig = fig + 1;
    end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%    
    % PART 1/c - PLEASE SEE NOTE AT END OF CODE
    % Overlay SURF descriptors on football.jpg
    if development
        % before
        figure(fig)
        imgs(BALL).sub_plot_image(2, 1, 1)
    end

    imgs(BALL).surf_descript(true)
    points = imgs(BALL).points;
    if development
        % after
        imgs(BALL).sub_plot_image(2, 1, 2)
        fig = fig + 1;
    end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%    
    % PART 1/d
    % Find the centroid of onion.png and draw a hull
    if development
        % b4
        figure(fig)
        imgs(ONION).sub_plot_image(2, 1, 1)
    end    
    imgs(ONION).lp_centroid(233)
    x = imgs(ONION).centroid(1);
    y = imgs(ONION).centroid(2);
    % TODO: figure out what a hull is
    
    if development
        % after
        hold on;
        plot(x, y, 'pr'); % hopefully you won't notice this...
        imgs(ONION).sub_plot_image(2, 1, 2);


        fig = fig + 1;
    end    
    
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    % Plot Modified images
    if development == false
        j = 1;
        for k = 1:8
            if rem(k, 2) ~= 1
                imgs(j).sub_plot_image(8, 2, k);
                j = j+1;
            end
        end
    end
    % Garbage Collection
    clear imgs
    
return;
    
function init()
% A Fresh Start is always nice...
close all
clear all
clc
return;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% NOTE
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% I installed the additional contrib folders for using isurf
% but get this error:

% The specified superclass
% 'OrientedScalePointFeature' contains a parse
% error or cannot be found on MATLAB's search
% path, possibly shadowed by another file with
% the same name.

% the help for isurf loads fine

% for some reason detectSURFFeatures doesnt work either
% THIS IS A LEGAL COPY OF MATLAB :-(

% ----------------------------------------------------------------------------------------------------------
% MATLAB Version: 8.1.0.604 (R2013a)
% MATLAB License Number: STUDENT
% Operating System: Mac OS X  Version: 10.10.2 Build: 14C1514 
% Java Version: Java 1.6.0_65-b14-466.1-11M4716 with Apple Inc. Java HotSpot(TM) 64-Bit Server VM mixed mode
% ----------------------------------------------------------------------------------------------------------
% MATLAB                                                Version 8.1        (R2013a)
% Simulink                                              Version 8.1        (R2013a)
% Control System Toolbox                                Version 9.5        (R2013a)
% DSP System Toolbox                                    Version 8.4        (R2013a)
% Image Processing Toolbox                              Version 8.2        (R2013a)
% Instrument Control Toolbox                            Version 3.3        (R2013a)
% Machine Vision Toolbox                                Version 3.4.0              
% Optimization Toolbox                                  Version 6.3        (R2013a)
% Signal Processing Toolbox                             Version 6.19       (R2013a)
% SimEvents                                             Version 4.3        (R2013a)
% Simulink Control Design                               Version 3.7        (R2013a)
% Statistics Toolbox                                    Version 8.2        (R2013a)
% Symbolic Math Toolbox

% SOLUTION: run a copy of OrientedScalePointFeature.m prior at start of
% main
