classdef ImageH < handle
    % ImageH contains all image manipulation properties and methods
    % in a re-usable Object for MTRN4230 image processing tasks
    
    properties
        % object variables
        image_name; % file name of image as a String/char
        image_data; % handle for loaded image, initialised in constructor
        x_dim; % image dimensions
        y_dim;
        % object utilities / constants
        pix_max = 256;
        pix_min = 0;
        centroid;
        points;
    end
    
    methods
        
        function obj = ImageH(image_name)
            % Class Constructor
            obj.image_name = image_name;
            obj.image_data = imread(image_name);
            image_dim = size(obj.image_data);
            obj.x_dim = image_dim(1);
            obj.y_dim = image_dim(2);
        end
        
        function obj = dev_plot(obj, figure_val)
            %Dev testing plot function
            figure(figure_val);
            imshow(obj.image_data);
        end
        
        function obj = sub_plot_image(obj, m, n, p)
            % Quick Sub Plot method
            subplot(m, n, p)
            subimage(obj.image_data)
            % For Scaling Function - currently breaks other functions
%             image_dim = size(obj.image_data);
%             obj.x_dim = image_dim(2);
%             obj.y_dim = image_dim(1);
        end
        
        function obj = tidy_scale(obj, width)
            % Scales images proportionally based on desired width in pixels
            scale_value = width/obj.x_dim;
            obj.image_data = imresize(obj.image_data, scale_value);
        end
        
        function obj = color_seg(obj, color, thresh, intensity)
            % Grey Scale all but the Chosen Color,
            % given a color difference threshold, intensity value
            % and a color where (R, G, B) = (1, 2, 3)
            rgb_image = obj.image_data;
            % Split into color bands.
            unwanted_color = zeros(2, 1);
            j = 1;
            for i = 1:3
                if i ~= color
                    unwanted_color(j) = i;
                    j=j+1;
                end
            end
            dband = rgb_image(:, :, color);
            uband1 = rgb_image(:, :, unwanted_color(1));
            uband2 = rgb_image(:, :, unwanted_color(2));
            % Iterate through pixels
            % and grey scale non-desired pixels
            for i=1:obj.x_dim
                for j=1:obj.y_dim
                    if dband(i, j) < (uband1(i, j)+thresh) && dband(i, j) < (uband2(i, j)+thresh) 
                        grey_pix = 0.3*dband(i,j) + 0.3*uband1(i, j) + 0.3*uband2(i, j);
%                         grey_pix = grey_pix/3;
                        grey_pix = grey_pix*intensity;
                        uband1(i,j)=grey_pix;
                        uband2(i,j)=grey_pix;
                        dband(i,j)=grey_pix;
                    end
                end
            end
            % return the modified image data
            if color == 1
                red = dband;
                green = uband1;
                blue = uband2;
            elseif color == 2
                red = uband1;
                green = dband;
                blue = uband2;
            elseif color == 3
                red = uband1;
                green = uband2;
                blue = dband;
            end
            obj.image_data = cat(3,red, green, blue);
        end
        
        function obj = trace(obj, hp_thresh, mask, pix_thresh)
            % Trace an outline of the image
            rgb_image = obj.image_data;
            gray_image = rgb2gray(rgb_image);
            % High Pass Block filter and Average Creator
            x_block = ceil(obj.x_dim/mask);
            y_block = ceil(obj.y_dim/mask);
            acc = 1;
%             averages = zeros(obj.x_dim, obj.y_dim);
            for i = 1:x_block:obj.x_dim
                for j = 1:y_block:obj.y_dim
                    if gray_image(i, j) > hp_thresh
                        for k = i:(i+x_block-1)
                            for m = j:(j+y_block-1)
%                                 averages(i, j) = averages(i, j) + gray_image(k, m);
                                gray_image(k, m) = obj.pix_max;
                                acc = acc + 1;
                            end
                        end
%                         for k = i:(i+x_block-1)
%                             for m = j:(j+y_block-1)
%                                 averages(k, m) = averages(i, j);
%                             end
%                         end
                    end
                end
            end
            % Second neighbour pixel comparison or compare to average in
            % zone. A block average method may perform faster/ better
            % however, for now I will move on. (see commented code)
            for i = 3:obj.x_dim-2
                for j = 3:obj.y_dim-2
                    % Threshold between neighbouring pixels
                    if gray_image(i, j) < obj.pix_max - pix_thresh
                        x_view = gray_image(i, j) - gray_image((i+2), j);
                        y_view = gray_image(i, j) - gray_image(i, (j+1));
                        x_rev = gray_image(i, j) - gray_image((i-2), j);
                        y_rev = gray_image(i, j) - gray_image(i, (j-1));
                        if x_view > pix_thresh || y_view > pix_thresh || x_rev > pix_thresh || y_rev > pix_thresh
                            rgb_image(i, j, 1) = 255;
                            rgb_image(i, j, 2) = 0;
                            rgb_image(i, j, 3) = 255;
                        end
                    end
                end
            end
            obj.image_data = rgb_image;
        end   
        % draw the outline
        function obj = trace_2(obj)
            edge_handle = edge(rgb2gray(obj.image_data));
            for i = 1:obj.x_dim
                for j = 1:obj.y_dim
                    if edge_handle(i, j) ~= 0
                        obj.image_data(i, j, 1) = 200;
                        obj.image_data(i, j, 2) = 0;
                        obj.image_data(i, j, 3) = 200;
                    end
                end
            end
        end
        
        function obj = surf_descript(obj, visible)
            %finds surf descriptors and optionally displays them 
            pcs_image = obj.image_data;
            gray_image = rgb2gray(pcs_image);
            obj.points = isurf(gray_image);
%             obj.points = obj.points(1:number:end);
            if visible
                for h = 1:length(obj.points)
                    x = ceil(obj.points(h).v);
                    y = ceil(obj.points(h).u);
                    % WORST STAR EVER
                    for i = (x-5):(x+5)
                        obj.image_data(i, y, 1) = 255;
                        obj.image_data(i, y, 2) = 0;
                        obj.image_data(i, y, 3) = 0;

                    end
                    for j = (y-5):(y+5)
                        obj.image_data(x, j, 1) = 255;
                        obj.image_data(x, j, 2) = 0;
                        obj.image_data(x, j, 3) = 0;
                    end
                    for a = 1:5
                        obj.image_data(x-a, y-a, 1) = 255;
                        obj.image_data(x+a, y-a, 1) = 255;
                        obj.image_data(x+a, y+a, 1) = 255;
                        obj.image_data(x-a, y+a, 1) = 255;
                        obj.image_data(x-a, y-a, 2) = 0;
                        obj.image_data(x+a, y-a, 2) = 0;
                        obj.image_data(x+a, y+a, 2) = 0;
                        obj.image_data(x-a, y+a, 2) = 0;
                        obj.image_data(x-a, y-a, 3) = 0;
                        obj.image_data(x+a, y-a, 3) = 0;
                        obj.image_data(x+a, y+a, 3) = 0;
                        obj.image_data(x-a, y+a, 3) = 0;            
                    end
                end
            end

        end
                    
        function obj = rotate_ninety(obj)
            % Rotates the image 90 degrees
            obj.image_data = imrotate(obj.image_data, 90);
            y_latch = obj.y_dim;
            obj.y_dim = obj.x_dim;
            obj.x_dim = y_latch;
        end
        
        function obj = lp_centroid(obj, lp_thresh)
            % performs a Low Pass Centroid analysis based on cutoff
            % threshhold
            gray_image = rgb2gray(obj.image_data);
            acc = 0;
            x_acc = 0;
            y_acc = 0;
            for i = 1:obj.x_dim
                for j = 1:obj.y_dim
                    if gray_image(i, j) > lp_thresh
                        acc = acc+1;
                        x_acc = x_acc + i;
                        y_acc = y_acc + j;

                    end
                end
            end
            x = ceil(x_acc/acc);
            y = ceil(y_acc/acc);
            % Should probably figure out how to return values
            % so I could plot it seperately
            % WORST STAR EVER
            for i = (x-5):(x+5)
                obj.image_data(i, y, 1) = 255;
                obj.image_data(i, y, 2) = 0;
                obj.image_data(i, y, 3) = 0;
 
            end
            for j = (y-5):(y+5)
                obj.image_data(x, j, 1) = 255;
                obj.image_data(x, j, 2) = 0;
                obj.image_data(x, j, 3) = 0;
            end
            for a = 1:5
                obj.image_data(x-a, y-a, 1) = 255;
                obj.image_data(x+a, y-a, 1) = 255;
                obj.image_data(x+a, y+a, 1) = 255;
                obj.image_data(x-a, y+a, 1) = 255;
                obj.image_data(x-a, y-a, 2) = 0;
                obj.image_data(x+a, y-a, 2) = 0;
                obj.image_data(x+a, y+a, 2) = 0;
                obj.image_data(x-a, y+a, 2) = 0;
                obj.image_data(x-a, y-a, 3) = 0;
                obj.image_data(x+a, y-a, 3) = 0;
                obj.image_data(x+a, y+a, 3) = 0;
                obj.image_data(x-a, y+a, 3) = 0;            
            end
            obj.centroid = [x, y];
        end
        
        function obj = rc_remove_trivial_blocks(obj, mask, top_pcnt)
            % Use surf descriptor to remove trivial areas
            pcs_points = obj.points;
                
            pcs_image = obj.image_data;

            % Keep it in rgb for now just in case - should add TOO much
            % time
            for i = 1:obj.x_dim
                for j = 1:obj.y_dim
                    pcs_image(i, j, 1) = 0;
                    pcs_image(i, j, 2) = 0;
                    pcs_image(i, j, 3) = 0;
                end
            end
            for k = 1:length(pcs_points)
                x = ceil(pcs_points(k).v);
                y = ceil(pcs_points(k).u);
                if x > obj.x_dim*top_pcnt
                    for i = (x-mask):(x+mask)
                        for j = (y-mask):(y+mask)
                            if i > 0 && i < (obj.x_dim-mask) && j > 0 && j < (obj.y_dim-mask)
                                pcs_image(i, j, 1) = obj.image_data(i, j, 1);
                                pcs_image(i, j, 2) = obj.image_data(i, j, 2);
                                pcs_image(i, j, 3) = obj.image_data(i, j, 3);
                            end
                        end
                    end
                end
            end
            obj.image_data = pcs_image;
        end
        
        function obj = surf_centroid(obj, visible)
            acc = 0;
            x_acc = 0;
            y_acc = 0;
            for k = 1:length(pcs_points)
                x_acc = x_acc + (pcs_points(k).v);
                y_acc = y_acc + (pcs_points(k).u);
                acc = acc + 1;
            end
            x = ceil(x_acc/acc);
            y = ceil(y_acc/acc);
            
            if visible
                % WORST STAR EVER
                for i = (x-5):(x+5)
                    obj.image_data(i, y, 1) = 255;
                    obj.image_data(i, y, 2) = 0;
                    obj.image_data(i, y, 3) = 0;

                end
                for j = (y-5):(y+5)
                    obj.image_data(x, j, 1) = 255;
                    obj.image_data(x, j, 2) = 0;
                    obj.image_data(x, j, 3) = 0;
                end
                for a = 1:5
                    obj.image_data(x-a, y-a, 1) = 255;
                    obj.image_data(x+a, y-a, 1) = 255;
                    obj.image_data(x+a, y+a, 1) = 255;
                    obj.image_data(x-a, y+a, 1) = 255;
                    obj.image_data(x-a, y-a, 2) = 0;
                    obj.image_data(x+a, y-a, 2) = 0;
                    obj.image_data(x+a, y+a, 2) = 0;
                    obj.image_data(x-a, y+a, 2) = 0;
                    obj.image_data(x-a, y-a, 3) = 0;
                    obj.image_data(x+a, y-a, 3) = 0;
                    obj.image_data(x+a, y+a, 3) = 0;
                    obj.image_data(x-a, y+a, 3) = 0;            
                end
            end
            

        end
            
% end methods
    end
% end classdef    
end