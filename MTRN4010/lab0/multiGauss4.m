function multiGauss4()

%Initialise
P = cell(1, 4);
P{1} = [4, 0; 0, 4];
P{2} = [4, 3; 3, 4];
%these don't work???
P{4} = [3, 3.98; 3.98, 4];
P{3} = [9, 5; 5, 4];

mean = [1,2];

%Utilities
Y = cell(1, 4);

%
for i = 1:4,
    [X1, X2] = meshgrid(linspace(-10, 10, 100)', linspace(-10, 10, 100)');
    X = [X1(:) X2(:)];
    Y = mvnpdf(X, mean, P{i});
    subplot(3, 1, i);
    surf(X1, X2, reshape(Y,100, 100)) 
end  


return
