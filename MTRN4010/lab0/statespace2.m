function statespace2()
problem_a = true;

%INITIAL CONDITIONS AND JUNK
if problem_a
    x = [0;0;0];
else
    x = [10;20;15];
end

a = [0.9, 0.1, 0.1; 0.1, 0.9, 0.3; 0, -0.3, 0.9];
b = [0;0;1];
c = [1, 0, 1];

%UTILITIES
x_history = [];
for k = 0:500
    u = impulse(k, problem_a);
    x = a*x + b*u
    x_history = [x_history, x];
end

figure();
for i = 1:3
    subplot(3, 1, i)
    plot(x_history(i,:));
    xlabel('time');ylabel('output');
end

return

function U = impulse(k, problem_a)
if problem_a
    if k > 10 && k < 20
        U = 1;
    elseif k > 21 && k < 30
        U = 2;
    else
        U = 0;
    end
else
    U = 0;
end
return