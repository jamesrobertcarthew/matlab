function [ ] = gauss_plotter( sigmasquared )
%gauss_plotter plots gauss distributions based of sigmasquared 
mean = 1;
x = [-10:.1: 11];
gauss = normpdf(x, mean, sqrt(sigmasquared));
figure(1);
plot(x, gauss);
end

