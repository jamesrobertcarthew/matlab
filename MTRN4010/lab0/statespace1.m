
function statespace1()
%INITALISE STUFF

%accumulative x values
x_history = []; 


%time stuff
start_time = 0; %probably in s
end_time = 7; % lets say s
dt = 1/1000; %1 ms 
time_frame = start_time: dt: end_time;

%position stuff
angular_velocity = 0; %probably in ms
angle_deg = 60; %deg
angle_rad = (angle_deg * pi) / 180; %rad

%first case, make U = 1
%will eventually be an impulse or something
U = 0;

x = [angle_rad, angular_velocity];

%RUN STUFF

for ms=time_frame,
    x = state_space(x, dt, U);
    x_history = [x_history, x(1)]; 
    U = impulse_function(ms);
    
end

%plot stuff
figure();
plot(x_history);
title('Problem 1');xlabel('time (ms)');ylabel('output (\Theta)');
return

%this function provides the next x value, based on the previous one
function new_x = state_space(x, dt, U)

A = 100; %rad/s^2
B = 2; % 1/s
C = 1; % rad/(s^2 * volt)

x_2 = x(2) + dt*(-A*sin(x(1)) - B*x(2) + C*U);
x_1 = x(1) + x(2)*dt;

new_x = [x_1, x_2];
return

function U = impulse_function(ms)
if ms > 1 && ms < 2
    U = 10;
elseif ms > 3 && ms < 4
    U = 20;
else
    U = 0;
end
return
    