

function LoadAndShowGyros()
    %---------------------------------------------------
    % **** LoadAndShowGyros.m ****
    % For Lab Task 001. (AAS.2015.S1)
    % This piece of code shows how to load a Matlab data file that contains 
    % IMU data (according to the structure format we use in our off-line data).
    % It also uses the time and gyros' measurements.
    % Jose Guivant and Alicia Robledo  


    % Read all the comments in the program
    % use the help in Matlab for learning the functionality of certain functions used in this example,
    % such as: load() ; plot() ; 

    %---------------------------------------------------
    % Here we load the file of interest.
    %  choose the dataset of interest, in this case the one in folder  "IMU_AAS003"
    subfolder  = 'IMU_AAS003';

    % '.\ImuData\',subfolder,'\IMU_MS01.mat'

    MyFile = 'IMU_MS01.mat';  
    load(MyFile) ;
    % now after I load the data, it will be contained in a variable named "IMU" (because it was saved using that name)
    %---------------------------------------------------

    % Each sample has an associated timestamp (sample time).
    % IMU.timesE is an array that contains the timestamps of the measurements, expressed using the system's clock.
    % In order to express it in seconds, we do the following scalling,
    times = double(IMU.timesE)/10000;      


    times = times - times(1) ;  % this is done in order to refer to t0=0, just for plotting purposes. 
    % make an array of dt values
    dt = zeros(length(times), 1);
    for i = 2:length(times)
        dt(i) = times(i)-times(i-1);
    end
        
        
    % IMU is a structure, whose field "DATAf(4:5,:)" contains the Gyros' measurements
    % IMU.DATAf(4,:) is Wx  (local Roll rate)
    % IMU.DATAf(5,:) is Wy  (local Pitch rate)
    % IMU.DATAf(6,:) is Wz  (local Yaw rate)
    % All measurements are expressed in radians/second

    Gyros = IMU.DATAf(4:6,:); 
    clear IMU ;

    %---------------------------------------------------
    % -- Here we plot the 3 angular rates (local, from gyros) ------


    figure(1) ;  clf() ; hold on ;
    k = 180/pi; % constant, useful for converting radian to degrees.
    plot(times,Gyros(1,:)*k,'b');
    plot(times,Gyros(2,:)*k,'r');
    plot(times,Gyros(3,:)*k,'g');
    hold off;
    legend({'Wx (roll rate)','Wy (pitch rate)','Wz (yaw rate)'});
    xlabel('time (in seconds)');
    ylabel('Angular rates (degrees/sec)');
    grid on ;
    zoom on;
    
    currentAttitude = [0,0,0];
    roll = zeros(length(dt), 1);
    pitch = zeros(length(dt), 1);
    yaw = zeros(length(dt), 1);
    
     gyro1 = biasDataAdjust(Gyros(1, :));
     gyro2 = biasDataAdjust(Gyros(2, :));
     gyro3 = biasDataAdjust(Gyros(3, :));
    
    for i = 1:length(dt)
        gyros = [gyro1(i), gyro2(i), gyro3(i)];
        currentAttitude = IntegrateOneStepOfAttitude(gyros, dt(i), currentAttitude);
        roll(i) = currentAttitude(1);
        pitch(i) = currentAttitude(2);
        yaw(i) = currentAttitude(3);
    end
    
    % Create Bias Vectors
    % Subtract Bias value @ t from roll, pitch, yaw @ time t
    % profit
    
%     roll = biasResultAdjust(roll, dt);
%     pitch = biasResultAdjust(pitch, dt);
%     yaw = biasResultAdjust(yaw, dt);
    
    figure(2); clf() ;hold on ;
    plot(times,roll(:)*k,'b');
    plot(times,pitch(:)*k,'r');
    plot(times,yaw(:)*k,'g');
    
    legend({'Roll','Pitch','Yaw'});
    xlabel('time (in seconds)')
    ylabel('Attitude (degrees)')

    %-----------------------------------------------------
    % You can use the help in Matlab for learning the functionality of certain 
    % functions used in this example, such as:
    % plot ; hold ; clf ; figure ; xlabel, ylabel ; grid ; zoom ; legend ;
    %-----------------------------------------------------
    % You can see how the integrated signals do look in file "HowShouldLookResultsofLabTask01.pdf" 
    %-----------------------------------------------------
    % If you have questions about this program, ask the lecturer:  j.guivant@unsw.edu.au
    %-----------------------------------------------------
return;

function adjustedData = biasDataAdjust(rawData)   
    
    avgError = mean(rawData(1:5000));
    adjustedData = rawData-avgError;
  % adjustedData = rawData;
return;

function adjustedData = biasResultAdjust(rawData, dt)
    stableLength = 5/dt(5); %ehh
    differs = 0;
    for i = 1:stableLength
        differs = differs + rawData(i)/i;
    end
    differs = differs/stableLength;
    for i = 1:length(rawData)
        rawData(i) = rawData(i) - differs*i;
    end
    adjustedData = rawData;
return;


function NewAttitude = IntegrateOneStepOfAttitude( gyros, dt, CurrentAttitude )
    % for a small delta time , dt
    % CurrentAttitude is the current (initial) attitude, in radians
    % gyros:vector with the gyros measurements, scaled in rad/sec
    ang = CurrentAttitude ; % current global Roll, Pitch, Yaw (at time t)
    wx = gyros(1); %local roll rate
    wy = gyros(2); %local pitch rate
    wz = gyros(3); %local yaw rate
    % -------------------------------
    cosang1=cos(ang(1)) ; 
    cosang2=cos(ang(2)) ; 
    sinang1=sin(ang(1)) ;
    roll = ang(1) + dt * (wx + (wy*sinang1 + wz*cosang1)*tan(ang(2))) ; %(*) 
    pitch = ang(2) + dt * (wy*cosang1 - wz*sinang1) ;
    yaw = ang(3) + dt * ((wy*sinang1 + wz*cosang1)/cosang2) ; %(*) 
    % -------------------------------
    NewAttitude= [roll,pitch,yaw]; 
    % new global Roll, Pitch, Yaw (at time t+dt) 
return ;











