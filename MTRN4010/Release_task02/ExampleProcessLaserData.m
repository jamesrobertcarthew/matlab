
% Example program, for processing saved laser scans. 
% Example source code, useful for Task02. 
% AAS - 2015.S1.
% Jose Guivant.




% note: Plotting of results is implemented via brute force approach. See other
% examples for better implementation.

%function MyProgram(file)
function MyProgram()
file ='Laser__2.mat';
load(file); data = XX; clear XX;

N = data.N;

for i=1:3:N,                        % in this example I skip some of them..
    ProcessScan(data.Ranges(:,i));
    pause(0.01) ;                   % wait for ~10ms
end;

return;
end


function ProcessScan(scan)

angles = [0:360]'*0.5* pi/180 ;         % associated angle for each range of scan


% separate range and intensity from measurements.
mask1FFF = uint16(2^13-1);
maskE000 = bitshift(uint16(7),13)  ;
intensities = bitand(scan,maskE000);
ranges    = single(bitand(scan,mask1FFF))*0.01;% range expressed in meters

% points, expressed in Cartesian. From the sensor's perpective.
X = cos(angles).*ranges;
Y = sin(angles).*ranges;    

% plot. "Brute force" plot (see note 1).
figure(1) ; clf(); hold on;
plot(X,Y,'b.');                     % all points
ii = find(intensities~=0);          % find those "pixels" that had intense reflection (>0)
plot(X(ii),Y(ii),'+r');             % plot highly reflective ones
axis([-10,10,0,20]);                % focuses plot on this region ( of interest in L220)


% to be done (by you)
OOIs = ExtractOOIs(ranges,intensities) ;
PlotOOIs(OOIs);

return;
end
function r = ExtractOOIs(ranges,intensities)
    r.N = 0;
    r.Centers = [];
    r.Sizes   = [];
    
    % your part....
    
return;
end
    
function PlotOOIs(OOIs)
    if OOIs.N<1, return ; end;
    % your part....
return;
end
    
% --------------------------------
% note 1: for a more efficient way of dynamically plotting, see example
% "ExampleUseLaserData.m", where we used graphical objects handles.
% --------------------------------
% Questions?  Ask the lecturer, j.guivant@unsw.edu.au
% --------------------------------








